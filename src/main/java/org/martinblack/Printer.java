package org.martinblack;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;
import java.util.stream.Collectors;

public class Printer extends TimerTask {

	private Map<String, List<Package>> packages;

	public Printer(Map<String, List<Package>> packages) {
		this.packages = packages;
	}

	@Override
	public void run() {
		printPackages(packages);
	}

	public static void printPackages(Map<String, List<Package>> packages) {
		System.out.println();
		System.out.println("--------------------");
		System.out.println("* Packages (zipCode totalWeight):");

		if (packages.isEmpty()) {
			System.out.println("* no packages");
		} else {
			Map<String, BigDecimal> temp = new HashMap<>();
			// sum values (weight) and place in temp map
			packages.forEach((key, value) -> {
				BigDecimal sum = value.stream().map(Package::getWeight).reduce(BigDecimal.ZERO, BigDecimal::add);
				temp.put(key, sum);
			});
			// order map by total weight
			Map<String, BigDecimal> foo = temp.entrySet().stream()
					.sorted((Map.Entry.<String, BigDecimal>comparingByValue().reversed()))
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
			// print
			foo.forEach((key, value) -> System.out.println("* " + key + " " + value.setScale(3, RoundingMode.HALF_UP)));
		}

		System.out.println("--------------------");
		System.out.println();
		System.out.println(App.MSG_INPUT);
	}
}
