package org.martinblack;

import java.math.BigDecimal;

public class Package {

	private BigDecimal weight;
	private String zipCode;

	public Package(BigDecimal weight, String zipCode) {
		this.weight = weight;
		this.zipCode = zipCode;
	}

	@Override
	public String toString() {
		return "Package{" +
				"weight=" + weight + "kg" +
				", zipCode='" + zipCode + '\'' +
				'}';
	}

	public BigDecimal getWeight() {
		return weight;
	}

	public String getZipCode() {
		return zipCode;
	}
}
