package org.martinblack;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Timer;
import java.util.regex.Pattern;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class App {

	public static final String MSG_INPUT = "Insert new package in format #.### ##### (weight zipCode) below.\n"
			+ "Optionally type \"quit\" to EXIT or \"print\" to PRINT package list.";

	private static final Pattern PATTERN_QUIT = Pattern.compile("quit", Pattern.CASE_INSENSITIVE);
	private static final Pattern PATTERN_PACKAGE = Pattern.compile("^\\s*(?=.*[1-9])\\d*(?:\\.\\d{1,3})?\\s(?:[0-9]{5})$");

	private static final long INTERVAL_PRINT = 60000L;

	/**
	 * Main application method.
	 *
	 * @param args Input command line parameters.
	 */
	public static void main(String[] args) {

		System.out.println();
		System.out.println("Welcome to Package manager!");

		Scanner scanner = new Scanner(System.in);
		Map<String, List<Package>> packages = new HashMap<>();

		// parse package files, if files are provided as arguments
		if (args.length > 0) {

			for (String arg : parseLaunchArguments(args)) {
				try {
					Scanner sc = new Scanner(new File(arg));
					while (sc.hasNext()) {
						Package newPackage = toPackage(sc.nextLine());
						if (newPackage != null) {
							packages.putIfAbsent(newPackage.getZipCode(), new ArrayList<>());
							packages.get(newPackage.getZipCode()).add(newPackage);
						}
					}
					sc.close();
					System.out.println("Package file " + arg + " successfully imported.");
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		}

		System.out.println();

		Timer printTimer = new Timer();
		printTimer.scheduleAtFixedRate(new Printer(packages), INTERVAL_PRINT, INTERVAL_PRINT);

		System.out.println(MSG_INPUT);

		// main application input loop
		while (true) {

			String line = scanner.nextLine();

			// quit
			if (PATTERN_QUIT.matcher(line).matches()) {
				System.exit(0);
			}

			// print package list
			if ("print".matches(line)) {
				Printer.printPackages(packages);
				continue;
			}

			Package newPackage = toPackage(line);
			if (newPackage != null) {
				packages.putIfAbsent(newPackage.getZipCode(), new ArrayList<>());
				packages.get(newPackage.getZipCode()).add(newPackage);
			}
		}
	}

	/**
	 * Checks if input string is in right format and valid and parses it into new package object. Returns null if format is wrong.
	 *
	 * @param packageString Input package text.
	 * @return New Package object or null if input is not valid.
	 */
	public static Package toPackage(final String packageString) {
		if (PATTERN_PACKAGE.matcher(packageString).matches()) {
			String[] parts = packageString.split(" ");
			BigDecimal weight = new BigDecimal(parts[0]);
			// validate weight
			if (weight.compareTo(BigDecimal.ZERO) > 0) {
				return new Package(weight, parts[1]);
			} else {
				System.out.println("Error: Package have to be heavier than 0 kg.");
				return null;
			}
		} else {
			System.out.println("Error: Wrong input format.");
			return null;
		}
	}

	private static String[] parseLaunchArguments(final String[] args) {
		Options options = new Options();

		Option input = new Option("i", "import", true, "path of file to import");
		options.addOption(input);

		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();

		try {
			CommandLine cmd = parser.parse(options, args);
			return cmd.getOptionValues("import");

		} catch (ParseException e) {
			System.out.println(e.getMessage());
			formatter.printHelp("utility-name", options);
			System.exit(1);
		}
		return null;
	}
}
