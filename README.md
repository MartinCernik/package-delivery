## Package management console app

### Building application
To build this application you will need JDK version 11 on your machine
and maven installed.
Then building itself can be done simply by running `mvn clean package`
from root directory (where pom.xml is placed).
Jar will be created in /target folder.

### Running application
After building the app you can run application jar from any command line
by command `java -jar target/package-delivery-1.0.0.jar`. Please modify the
relative path to jar file accordingly.

### Importing default data from files
You can import one or multiple files by passing them as program arguments.
Argument for each file looks like this: `-i file.txt` or `--import file.txt`.
Files will be parsed and imported if they have valid format.
Lines should have the same format as is input format of each package.

Example (one file): `java -jar ${path_to_jar} -i file-to-import.txt`<br>
Example (two files file): `java -jar ${path_to_jar} -i file-to-import.txt -i another-file.txt`

Example of file content:
```
1.2 15000
0.52 22444
.156 11100
12.96 12001
2.77 22444
```

### User input validation
Validation of user's input is done by using regex for format and > 0 validation for package weight.
In case of bad format, user will be prompted by simple custom error message.
